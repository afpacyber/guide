package fr.afpa.guide;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class HomeActivity extends AppActivity {

    // déclaration de propriétés (ou variable)
    private Button buttonRestaurant;
    private Button buttonHotel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        // récupération des Views (vues)
        buttonRestaurant = findViewById(R.id.buttonRestaurant);
        buttonHotel = findViewById(R.id.buttonHotel);

        buttonRestaurant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO: lancer l'écran ListingActivity
                Intent intentRestaurant = new Intent(HomeActivity.this, ListingActivity.class);
                intentRestaurant.putExtra("isRestaurant", true);
                startActivity(intentRestaurant);
            }
        });

        buttonHotel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO: lancer l'écran ListingActivity
                Intent intentHotel = new Intent(HomeActivity.this, ListingActivity.class);
                intentHotel.putExtra("isRestaurant", false);
                startActivity(intentHotel);
            }
        });
    }
}

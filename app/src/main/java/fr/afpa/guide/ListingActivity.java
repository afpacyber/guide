package fr.afpa.guide;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import fr.afpa.guide.models.Hotel;
import fr.afpa.guide.models.Restaurant;

public class ListingActivity extends AppActivity {

    // déclaration
    private TextView textViewTitle;
    private ListView listViewData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing);

        // récupréation des vues (View)
        textViewTitle = findViewById(R.id.textViewTitle);
        listViewData = findViewById(R.id.listViewData);

//        // création d'un Tableau de String (de chaîne de caractères)
//        String[] restaurants = {"Mac DO","Quick","KFC","Burger King","Mac DO","Quick","KFC","Burger King"};
//
//        // gestion de l'affichage des Items (des données = nom des restaurants)
//        listViewData.setAdapter(new ArrayAdapter<String>(ListingActivity.this, // Context
//                android.R.layout.simple_list_item_1, // Layout
//                restaurants)); // Données

        // récupération de isRestaurant
        if(getIntent().getExtras() != null) {
            boolean isRestaurant = getIntent().getExtras().getBoolean("isRestaurant");

            if(isRestaurant) {
                // TODO : afficher Les Restaurants
                textViewTitle.setText(R.string.listing_restaurant_title);

                // création d'une liste de Restaurant
                final List<Restaurant> restaurantList = new ArrayList<>();
                restaurantList.add(new Restaurant(
                        "Mac do",
                        "Fast Food",
                        "info@macdo.fr",
                        "0102030405",
                        "http://www.macdo.fr",
                        "https://www.bfmtv.com/i/0/0/541/50c1f47eac0b27d758236b831be2e.jpeg" // Indiquez une url vers une image hébergé sur Internet
                        )
                );
                restaurantList.add(new Restaurant(
                        "Quick",
                        "Fast Food",
                        "info@macdo.fr",
                        "0102030405",
                        "http://www.macdo.fr",
                        "https://media-cdn.tripadvisor.com/media/photo-p/0b/de/93/51/quick-flemalle.jpg" // Indiquez une url vers une image hébergé sur Internet
                        )
                );

                listViewData.setAdapter(new RestaurantAdapter(ListingActivity.this,
                        R.layout.item_restaurant,
                        restaurantList));

                listViewData.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Restaurant item = restaurantList.get(position);

                        Intent intentDetails = new Intent(ListingActivity.this, DetailsActivity.class);

                        // passage des informations
                        intentDetails.putExtra("name", item.getName());
                        intentDetails.putExtra("category", item.getCategory());
                        intentDetails.putExtra("email", item.getEmail());
                        intentDetails.putExtra("phone", item.getPhone());
                        intentDetails.putExtra("url", item.getUrl());
                        intentDetails.putExtra("image", item.getImage());

                        startActivity(intentDetails);
                    }
                });
            } else {
                // TODO : afficher Les Hôtels
                textViewTitle.setText(R.string.listing_hotel_title);

                // création d'une liste de Restaurant
                List<Hotel> hotelList = new ArrayList<>();
                hotelList.add(new Hotel(
                                "Ibis",
                                "Discount",
                                "info@ibis.fr",
                                "0102030405",
                                "http://www.accorshotel.fr",
                                "" // Indiquez une url vers une image hébergé sur Internet
                        )
                );
                hotelList.add(new Hotel(
                                "Hilton",
                                "Luxe",
                                "info@hilton.com",
                                "0102030405",
                                "http://www.hilton.com",
                                "" // Indiquez une url vers une image hébergé sur Internet
                        )
                );

                listViewData.setAdapter(new HotelAdapter(ListingActivity.this,
                        R.layout.item_hotel,
                        hotelList));
            }
        }
    }
}

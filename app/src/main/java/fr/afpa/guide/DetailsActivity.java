package fr.afpa.guide;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class DetailsActivity extends AppActivity {

    private ImageView imageViewPhoto;
    private TextView textViewTitle;
    private TextView textViewCategory;
    private Button buttonEmail;
    private Button buttonPhone;
    private Button buttonUrl;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        imageViewPhoto = (ImageView) findViewById(R.id.imageViewPhoto);
        textViewTitle = (TextView) findViewById(R.id.textViewTitle);
        textViewCategory = (TextView) findViewById(R.id.textViewCategory);
        buttonEmail = (Button) findViewById(R.id.buttonEmail);
        buttonPhone = (Button) findViewById(R.id.buttonPhone);
        buttonUrl = (Button) findViewById(R.id.buttonUrl);

        if(getIntent().getExtras() != null) {
            String name = getIntent().getExtras().getString("name");
            String category = getIntent().getExtras().getString("category");
            String email = getIntent().getExtras().getString("email");
            String phone = getIntent().getExtras().getString("phone");
            String url = getIntent().getExtras().getString("url");
            String image = getIntent().getExtras().getString("image");

            textViewTitle.setText(name);
            textViewCategory.setText(category);
            buttonEmail.setText(email);
            buttonPhone.setText(phone);
            buttonUrl.setText(url);

            // affichage l'image
            Picasso.get().load(image).into(imageViewPhoto);
        }
    }

    public void sendEmail(View view) {
        Intent intentEmail = new Intent(Intent.ACTION_SEND);
        intentEmail.setType("message/rfc822");
        intentEmail.putExtra(Intent.EXTRA_EMAIL, new String[] {
                buttonEmail.getText().toString(),
                "mathieu.masset@vivaneo.fr"
        });
        intentEmail.putExtra(Intent.EXTRA_CC, new String[] { "mickael.masset@vivaneo.fr" });
        intentEmail.putExtra(Intent.EXTRA_SUBJECT, "le sujet");
        intentEmail.putExtra(Intent.EXTRA_TEXT, "le message");
        startActivity(Intent.createChooser(intentEmail, "Partage :"));
    }

    public void call(View view) {
        Intent intentPhone = new Intent(Intent.ACTION_CALL);
        intentPhone.setData(Uri.parse("tel:"+buttonPhone.getText().toString()));

        // demande de permission CALL_PHONE
        if(ContextCompat.checkSelfPermission(DetailsActivity.this, Manifest.permission.CALL_PHONE)
                == PackageManager.PERMISSION_DENIED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[] { Manifest.permission.CALL_PHONE }, 100);
            }
        } else {
            startActivity(intentPhone);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == 100) {
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED) { // accepté
                // lancer l'appel téléphonique
                buttonPhone.performClick(); // simule un click
            } else { // réfusé
                // afficher une erreur
                showToast("Permission refusé");
            }
        }
    }

    public void showWebSite(View view) {
        Intent intentUrl = new Intent(Intent.ACTION_VIEW);
        intentUrl.setData(Uri.parse(buttonUrl.getText().toString()));
        startActivity(intentUrl);
    }
}

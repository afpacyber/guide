package fr.afpa.guide;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import fr.afpa.guide.models.Restaurant;

public class RestaurantAdapter extends ArrayAdapter<Restaurant> {

    private int resId;

    public RestaurantAdapter(Context context, int resource, List<Restaurant> objects) {
        super(context, resource, objects);
        resId = resource; // R.layout.item_restaurant
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder myViewHolder = null; // déclaration

        if(convertView == null) {
            // utilise la leyout R.layout.item_restaurant
            convertView = LayoutInflater.from(getContext()).inflate(resId, null);

            myViewHolder = new ViewHolder(); // création de l'instance = utilisation de mémoire

            myViewHolder.textViewTitle = convertView.findViewById(R.id.textViewTitle);
            myViewHolder.textViewCategory = convertView.findViewById(R.id.textViewCategory);

            convertView.setTag(myViewHolder);// enregistrement de l'instance du ViewHolder
        } else {
            myViewHolder = (ViewHolder) convertView.getTag();
        }

        Restaurant item = getItem(position);

        myViewHolder.textViewTitle.setText(item.getName());
        myViewHolder.textViewCategory.setText(item.getCategory());

        return convertView;
    }

    private class ViewHolder {
        public TextView textViewTitle;
        public TextView textViewCategory;
    }
}
